export const factions = [
  { code: 'union', name: 'Galactic Union' },
  { code: 'trades', name: 'Traders\' Fleet' },
  { code: 'rebels', name: 'Rebel Faction' },
];

export const techLevels = [
  { level: 1, name: 'Agrarian', desription: 'Farming, iron work' },
  { level: 2, name: 'Post-medieval', desription: 'Books, feudalism' },
  { level: 3, name: 'Industrial', desription: 'Trains, communication' },
  { level: 4, name: 'Nuclear', desription: 'Planes, power' },
  { level: 5, name: 'Digital', desription: '(pessimistic modern): Internet, rockets, mass communication' },
  { level: 6, name: 'Robotic', desription: '(optimistic modern): Drones, interplanetary transport, AI' },
  { level: 7, name: 'Microtech', desription: 'Interstellar transport, advanced shielding, life support' },
  { level: 8, name: 'Future', desription: 'Secretive developing technologies: teleportation, time machines, weather control' },
];

export const climates = [
  { code: 'ice', name: 'Ice', color: '#befefb' },
  { code: 'temperate', name: 'Temperate', color: '#2db76e' },
  { code: 'tropical', name: 'Tropical', color: '#54c901' },
  { code: 'water', name: 'Water', color: '#2b6dcf' },
  { code: 'desert', name: 'Desert', color: '#d7c500' },
];

export const planetInclinations = [
  { code: 'military', name: 'Military' },
  { code: 'farming', name: 'Farming' },
  { code: 'mining', name: 'Mining' },
  { code: 'party', name: 'Party' },
];

export const stationInclinations = [
  { code: 'military', name: 'Military' },
  { code: 'mining', name: 'Mining' },
  { code: 'party', name: 'Party' },
  { code: 'repair', name: 'Repair' },
];

export const cargoTypes = [
  { code: 'general', name: 'General cargo', defaultValue: 1 },
  { code: 'water', name: 'Water', defaultValue: 2 },
  { code: 'weapons', name: 'Weapons', defaultValue: 10 },
];

export const roomTypes = [
  { code: 'weapons', name: 'Weapons bay', icon: '🔫' },
  { code: 'bridge', name: 'Bridge', icon: '️️🎛️' },
  { code: 'shields', name: 'Shields', icon: '️🛡️' },
  { code: 'bunk', name: 'Bunk', icon: '🛏️' },
  { code: 'reactor', name: 'Reactor', icon: '️🔋' },
  { code: 'hold', name: 'Cargo hold', icon: '📦' },
];

export default {
  cargoTypes,
  climates,
  factions,
  planetInclinations,
  roomTypes,
  stationInclinations,
  techLevels,
};
