import { rand } from '../random';
import generatePlanet from './planet';
import generateStation from './station';

const regionWidth = 3000;
const regionHeight = 3000;
const cellWidth = 300;
const cellHeight = 300;
const maxLocationSize = 30;

const generatedRegions = [];

/**
 * Generate the region (x,y)
 * This amounts to the area [(x - 0.5) * 1000, (y - 0.5) * 1000, (x + 0.5) * 1000, (y + 0.5) * 1000]
 */
export default function generateRegion(cx, cy) {
  const regionId = `${cx}-${cy}`;
  // Check the region hasn't already been generated
  if (generatedRegions.includes(regionId)) return [];

  const locations = [];

  // Generate a regular grid of square cells covering the requested region
  for (let x = cx * regionWidth; x < (cx + 1) * regionWidth; x += cellWidth) {
    for (let y = cy * regionHeight; y < (cy + 1) * regionHeight; y += cellHeight) {
      // In some of those regions, generate locations
      if (Math.random() < 0.7) {
        // Place the location randomly within the circular region of the cell,
        // with a margin of the maximum possible size
        const theta = rand(0, 359);
        const d = rand(0, cellWidth / 2) - maxLocationSize;

        const location = {
          id: `${x}-${y}`,
          cell: { x, y },
          x: x + (cellWidth / 2) + (d * Math.sin(theta)),
          y: y + (cellHeight / 2) + (d * Math.cos(theta)),
        };

        location.type = Math.random() > 0.9 ? 'station' : 'planet';

        if (location.type === 'planet') {
          locations.push({
            ...location,
            ...generatePlanet(),
          });
        } else if (location.type === 'station') {
          locations.push({
            ...location,
            ...generateStation(),
          });
        }
      }
    }
  }

  // Mark this region as generated
  generatedRegions.push(regionId);

  // Return the new locations
  return locations;
}
