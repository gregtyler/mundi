import { randomNormal } from 'd3-random';
import { factions, stationInclinations as inclinations } from '../data';
import { arrayRand } from '../random';
import generateName from './name';

const stationPopulationDistribution = randomNormal(200, 150);

const generateStation = () => ({
  name: generateName('<station>').name,
  size: 3,
  breathable: true,
  inclination: arrayRand(inclinations),
  allegiance: arrayRand(factions),
  population: Math.max(Math.round(stationPopulationDistribution()), 0),
  techLevel: Math.round(arrayRand([5, 6, 7, 8])),
});

export default generateStation;
