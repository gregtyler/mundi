export default {
  suitability() {
    return false;
  },
  steps: [
    {
      id: '_start',
      text: ({ ship, location }) => `The ${ship.name} lands on ${location.name} and find yourself on a narrow animal track.`,
      options: [
        { label: 'Turn left', to: 'boulder' },
        { label: 'Turn right', to: 'alien' },
      ],
    }, {
      id: 'boulder',
      text: 'You take the path to the left. A boulder blocks your way.',
      options: [
        { label: 'Shoot it with a laser', to: 'escape' },
        { label: 'Climb over it', to: () => (Math.random() > 0.5 ? 'boulder-climb' : 'boulder-fall') },
      ],
    }, {
      id: 'escape',
      text: 'You pull out your blaster and blow the boulder to smithereens. Behind it the path leads you back to your ship.',
    }, {
      id: 'boulder-climb',
      text: '[success You climb up and over the boulder, spotting your ship over the top.]',
    }, {
      id: 'boulder-fall',
      text: 'You attempt to climb over the rock, but your dexterity is weak and you slip on the smooth face.\n[danger -10 Health]',
      effect: ({ player }) => { player.updateHealth(-10); },
      options: [
        { label: 'Shoot the boulder with a laser', to: 'escape' },
      ],
    }, {
      id: 'alien',
      text: ({ location }) => `You take the path to the right and encounter a ${location.name} native.\n"You're going the wrong way", they say. "You should have turned left."`,
      options: [
        { label: 'Go back and turn left', to: 'boulder' },
        { label: '"I DEMAND SATISFACTION!"', to: 'fight' },
      ],
    }, {
      id: 'fight',
      text: 'You pull out your blaster and shoot the native who explodes with the impact.\nContinuing down the path you find yourself in a small settlement. The villagers turn to face you as you stumble through, their comrade\'s blood still sprayed across your uniform.\n"Arundel", one yells, piecing together. "Oh God, my son. My only son!"\nAs you look around you find yourself at the end of a crevasse. Arundel was right, this is completely the wrong way.',
      effect: ({ dispatch }) => { dispatch('map/setPlanetAlignment', 'hostile'); },
      options: [
        { label: 'Go back and turn left', to: 'boulder' },
      ],
    },
  ],
};
