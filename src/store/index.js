import Vue from 'vue';
import Vuex from 'vuex';
import app from './modules/app';
import log from './modules/log';
import map from './modules/map';
import ship from './modules/ship';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: true,
  modules: {
    app,
    log,
    map,
    ship,
  },
});

// Generate the root area
(async function init() {
  await store.dispatch('map/generate', { x: 0, y: 0 });

  // Place the ship
  const location = store.state.map.locations[Math.floor(store.state.map.locations.length / 2)];
  await store.dispatch('log/setLocation', location.id);

  // Initialise the app
  await store.dispatch('app/init');
}());

export default store;
