/* eslint-disable no-param-reassign */
export default {
  namespaced: true,
  state: {
    isLoaded: false,
  },
  actions: {
    init({ commit }) {
      commit('init');
    },
  },
  mutations: {
    init(state) {
      state.isLoaded = true;
    },
  },
};
