import Vue from 'vue';
import generateRegion from '../../lib/generators/spacemap';

/* eslint-disable no-param-reassign */
export default {
  namespaced: true,
  state: {
    regions: [],
    locations: [],
  },
  actions: {
    generate({ commit }, { x, y }) {
      const locations = generateRegion(x, y);

      commit('addRegion', { x, y });
      commit('addLocations', locations);
    },
    completeNarrative({ commit }, locationId) {
      commit('completeNarrative', locationId);
    },
  },
  mutations: {
    addRegion(state, region) {
      state.regions.push(region);
    },
    addLocations(state, locations) {
      locations.forEach((location) => {
        state.locations.push(location);
      });
    },
    setNarrative(state, { locationId, narrative }) {
      const location = state.locations.find(l => l.id === locationId);
      Vue.set(location, 'narrative', {
        steps: narrative.steps,
        isComplete: false,
      });
    },
    completeNarrative(state, locationId) {
      const location = state.locations.find(l => l.id === locationId);
      location.narrative.isComplete = true;
    },
  },
};
