/* eslint-disable no-param-reassign */
const initialState = [
  { code: 'water', quantity: 2 },
  { code: 'general', quantity: 5 },
  { code: 'weapons', quantity: 0.5 },
];

export default {
  namespaced: true,
  state: initialState,
  mutations: {
    add(state, { code, quantity }) {
      const stock = state.find(item => item.code === code);
      if (stock) {
        stock.quantity += quantity;
      } else {
        state.push({
          code,
          quantity,
        });
      }
    },
    remove(state, { code, quantity }) {
      const stock = state.find(item => item.code === code);
      if (stock.quantity > quantity) {
        stock.quantity -= quantity;
      } else {
        state.splice(state.indexOf(stock), 1);
      }
    },
  },
};
