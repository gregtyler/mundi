/* eslint-disable no-param-reassign */
const initialState = [
  {
    id: '8w9ur3ur',
    name: 'Foge',
    race: 'human',
    profession: 'pilot',
    stamina: 4,
    charisma: 7,
    driving: 14,
    skills: ['gambler', 'quick-witted'],
  },
];

export default {
  namespaced: true,
  state: initialState,
  mutations: {},
};
